import React, {Component} from 'react';

class Todo extends Component {

    constructor(){
        super();
        this.state = {
            userInput: '',
            items: []
        }
    }

    onChange(event){
        this.setState({
            userInput: event.target.value
        }, //() => console.log(this.state.userInput)
        );
    }

    add(event){
        //sinon page qui va se reloder
        event.preventDefault();
        this.setState({
            //réinitialise champ input à vide en gros
            userInput: '',
          //es6 => tableau mutaable
          items: [...this.state.items, this.state.userInput]
        }, () => console.log(this.state.items)
        )
    }

    remove(event){
        event.preventDefault();
        const array = this.state.items;
        const index = array.indexOf(event.target.value);
        //Supprime une ligne à partir de l'élément donc l'élément dans ce cas
        array.splice(index, 1);
        //item = array aapres la supp (constante du coups) ?
        this.setState({
            items: array
        })
    }

    renderTodos(){
        // fera une loop sur items foreach JS en gros
        return this.state.items.map((item) => {
            return (
                <div className="list-group-item" key={item}>
                    {item} | <button onClick={this.remove.bind(this)}>X</button>
                </div>
            );
        });
    }

    render(){
        return(
            <div>
                <h1 align="center">Ma Todo List</h1>
                <div>
                    <form className="form-row align-item-center">
                        <input className="form-control mb-2"
                            value={this.state.userInput}
                            type="text"
                            placeholder="Renseignez todo"
                            onChange={this.onChange.bind(this)}
                        />
                        <button className="btn btn-primary" onClick={this.add.bind(this)}>Ajouter</button>
                    </form>
                    <div className="list-group">
                        {this.renderTodos()}
                    </div>
                </div>
            </div>
        );
    }
}

export default Todo;