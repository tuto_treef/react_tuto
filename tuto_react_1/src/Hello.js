import React, {Component} from 'react';
//import logo from './logo.svg';
//import './App.css';

class Hello extends Component{

    constructor() {
        super();
        this.state = {
            count: 0
        }
    }

    Add_1() {
        this.setState({
                count: this.state.count + 1
        });
    }

    Remove_1() {
        this.setState({
            count: this.state.count - 1
        })
    }

    render()
    {
        return (
            //Je ne peux pas rendre plusieurs élément mais je peux imbriqué les éléments
            <div>
                <div className=" ">
                    <h1>Hello  {this.props.world }</h1>

                    <div>
                        <p>Mon compteur = {this.state.count}</p>
                        <button onClick={ () => this.Add_1() }>Incrémente de 1</button>
                        <button onClick={ this.Remove_1.bind(this) }>décrémente 1</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Hello;